#include <httpserver.hpp>
#include <iostream>

using namespace httpserver;

class recognize_resource : public http_resource {
public:
    std::shared_ptr<http_response> render(const http_request&) {
        std::cout << "Recognize handler: received request";
        return std::shared_ptr<http_response>(new string_response("Hello, World!"));
    }
};

int main(int argc, char** argv) {
    std::cout << "Starting recognize web-server on :8080";

    recognize_resource rr;
    
    webserver ws = create_webserver(8080);
    ws.register_resource("/", &rr);
    ws.start(true);
    
    return 0;
}
