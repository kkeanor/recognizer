CXX = g++ # or any other C++ compiler
CXXFLAGS = -std=c++17 -Wall -Wextra # or any other desired flags for C++17 and MacOS
LDFLAGS = -L/usr/local/lib -lhttpserver # assuming httserver library and boost libraries are installed in /usr/local/lib


compile: build
	$(CXX) $(CXXFLAGS) main.o -o main $(LDFLAGS)

build: main.cpp
	$(CXX) $(CXXFLAGS) -c main.cpp

clean:
	rm main | true
	rm main.o | true